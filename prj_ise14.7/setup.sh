#!/bin/sh

# Create a link to

#   gitlab.com/csantosb/eda-common/xilinx-ise/Makefile

# as a supporting, common file to all simulations

[ -e Makefile ] && rm Makefile
ln -s $HOME/Projects/perso/eda-common/xilinx-ise/Makefile.edacommon.ise Makefile

! [ -e local.mk ] && cp $HOME/Projects/perso/eda-common/xilinx-ise/local.mk local.mk

echo "Done."
